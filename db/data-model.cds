namespace my.comp;

entity FunctionalCompetency {
    key FuncCompId   : String;
        Description  : localized String;
        StartDate    : DateTime;
        EndDate      : DateTime;
        ParentId     : String;
        ToCompetency : Association[ * ] to Competency
                           on ToCompetency.FuncCompId = FuncCompId;
}

entity Competency {
    key CompId                 : String;
    key FuncCompId             : String;
        Description            : localized String;
        StartDate              : DateTime;
        EndDate                : DateTime;
        RatingType             : String;
        ToFunctionalCompetency : Association[1] to FunctionalCompetency
                                     on ToFunctionalCompetency.FuncCompId = FuncCompId;
}

entity User {
    key UserId     : String;
        Initials   : String;
        FirstName  : String;
        LastName   : String;
        Title      : String;
        Division   : String;
        Department : String;
        ReportsTo  : String;
        Location   : String;
        Email      : String;
        Phone      : String;
        Photo      : String;
}

entity UserFunctionalCompetency {
    key UserId     : String;
    key FuncCompId : String;
        StartDate  : DateTime;
        EndDate    : DateTime;
    ToFunctionalCompetency : Association[1] to FunctionalCompetency
                                     on ToFunctionalCompetency.FuncCompId = FuncCompId;
        Progress   : Integer;
}

entity UserCompetency {
    key UserId    : String;
    key CompId    : String;
        FuncCompId : String;
        StartDate : DateTime;
        EndDate   : DateTime;
        Rating    : Integer;
    ToCompetency : Association[1] to Competency
                                     on ToCompetency.CompId = CompId;
}

entity Photo {
    key PhotoId   : String;
        UserId    : String;
        PhotoName : String;
        Photo     : String;
        MimeType  : String;
}

entity WorkingExperience {
    key ProjectId  : String;
    key UserId     : String;
    key CustomerId : String;
    ProjectName    : String;
    StartDate      : DateTime;
    EndDate        : DateTime;
    Description    : String;
    Role           : String;
}