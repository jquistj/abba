using my.comp as my from '../db/data-model';

service CatalogService {
  entity FunctionalCompetencySet as projection on my.FunctionalCompetency;
  entity CompetencySet as projection on my.Competency;
  entity UserSet @readonly as projection on my.User;
  entity UserFunctionalCompetencySet @readonly as projection on my.UserFunctionalCompetency;
  entity UserCompetencySet @readonly as projection on my.UserCompetency;
  entity PhotoSet @readonly as projection on my.Photo;
  entity WorkingExperienceSet as projection on my.WorkingExperience;  
}